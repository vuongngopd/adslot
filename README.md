## Description
This repo uses React/Redux and Redux-Saga to attempt Adslot frontend challenge.  
Features:  
- Search functionality is based on partial match between query terms and site names or category.  
- Leverage mock data for search; however, function could be extended by using saga to get data from API.  
- Unit test and Component tests are provided. Run `npm run test:watch` for TDD.  
- Use react styled components for styling rather than css or SASS. This provides opportunity to encapsulate styling to component base.  

## Quick start

1. Clone this repo using `git clone https://vuongngopd@bitbucket.org/vuongngopd/adslot.git`
1. Run `npm run setup` to install dependencies and clean the git repo.<br />
   *Auto-detect `yarn` for installing packages by default, if you wish to force `npm` usage do: `USE_YARN=false npm run setup`*<br />
   *At this point you can run `npm start` to see the example app at `http://localhost:3000`.*

## Documentation

- [**The Hitchhikers Guide to `react-boilerplate`**](docs/general/introduction.md): An introduction for newcomers to this boilerplate.
- [Overview](docs/general): A short overview of the included tools
- [**Commands**](docs/general/commands.md): Getting the most out of this boilerplate
- [Testing](docs/testing): How to work with the built-in test harness
- [Styling](docs/css): How to work with the CSS tooling
- [Your app](docs/js): Supercharging your app with Routing, Redux, simple
  asynchronicity helpers, etc.