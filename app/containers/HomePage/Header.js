import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
  display: flex ;
  flex-flow: column;
  height: 450px;
  justify-content: center;
  align-items: center;
`;

const HWrapper = styled.h1`
  color: #919294;
  font-size: 4rem;
  letter-spacing: 3px;
  @media (max-width: 640px) {
    font-size: 3rem;
    letter-spacing: 2px;
  }
`;

const PWrapper = styled.p`
  color: grey;
  font-size: 1.25rem;
  font-weight: 300;
  text-align: center;
  @media (max-width: 640px) {
    margin-left: 10px;
    margin-right: 10px;
  }
`;

function Header() {
  return (
    <Wrapper>
      <HWrapper>Adslot Search</HWrapper>
      <PWrapper>Instant Search for Sites by name or category</PWrapper>
    </Wrapper>
  );
}

export default Header;
