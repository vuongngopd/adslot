/**
 * Test the HomePage
 */

import React from 'react';
import { mount } from 'enzyme';
import * as router from 'react-router';

import TopBar from 'components/TopBar';
import IconButton from 'components/IconButton';
import Search from 'components/Icon/Search';

import HomePage from '../index';

describe('<HomePage />', () => {
  it('should render TopBar with Search Icon', () => {
    const renderedComponent = mount(
      <HomePage />
    );
    expect(renderedComponent.find(TopBar).length).toBe(1);
    expect(renderedComponent.find(Search).length).toBe(1);
  });

  it('should navigate with click', () => {
    const spy = jest.fn();
    router.browserHistory = { push: spy };
    const renderedComponent = mount(
      <HomePage />
    );
    renderedComponent.find(IconButton).simulate('click');
    expect(spy).toHaveBeenCalledWith('/search');
  });
});
