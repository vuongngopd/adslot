/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 */

import React from 'react';
import { browserHistory } from 'react-router';
import Helmet from 'react-helmet';

import TopBar from 'components/TopBar';
import IconButton from 'components/IconButton';
import Search from 'components/Icon/Search';
import Header from './Header';

class HomePage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <div>
        <TopBar
          right={<div><IconButton icon={<Search />} label="Search" onClick={() => browserHistory.push('/search')} /></div>}
        />
        <article>
          <Helmet
            title="Home Page"
            meta={[
              { name: 'description', content: 'Adslot Homepage' },
            ]}
          />
          <Header />
        </article>
      </div>
    );
  }
}

HomePage.propTypes = {
};

export default HomePage;
