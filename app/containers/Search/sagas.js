import { put, select, takeEvery } from 'redux-saga/effects';
import uniqBy from 'lodash/uniqBy';
import { QUERY } from './constants';
import { sites, categories } from './mock';
import { makeSelectQuery } from './selectors';
import { setResults } from './actions';


// Individual exports for testing
export function* mockSearch() {
  const query = yield select(makeSelectQuery()); // Get query from store
  const terms = query.split(',').map((x) => x.toLowerCase().trim());
  // Mock API obj, this should be an asyn call to backend to get result
  const res = mergeSiteWithCategory(sites, categories);
  const results = terms
          .map((q) => searchBySites(q, res))
          .reduce((prev, cur) => prev.concat(cur));
  yield put(setResults(uniqBy(results), 'id'));
}

/**
 * Root saga manages watcher
 */
export function* watchSearch() {
  yield takeEvery(QUERY, mockSearch);
}

/**
 * Private funcs
 */

/**
 * Get sites based on site description and siteName from API object
 *
 * @param {q} string Single query term
 *
 * @param {obj} object Object from API call
 */
export function searchBySites(q, list) {
  // Filter by siteName and category
  // Partial match with text
  return list.concat([])
    .filter((obj) => obj.siteName.toLowerCase().includes(q) || obj.categories.findIndex((x) => x.includes(q)) > -1);
}

/**
 * Embeb category names in sites
 *
 * @param {s} array Sites
 *
 * @param {c} array Categories
 */
export function mergeSiteWithCategory(s, c) {
  return s.map((site) => {
    const cp = Object.assign({}, site);
    cp.categories = getSiteCategories(site, c);
    return cp;
  });
}

/**
 * Get site category name
 *
 * @param {site} object Single site
 *
 * @param {c} array Categories
 */
export function getSiteCategories(site, c) {
  return c
    .filter((category) => site.categoryIds.findIndex((x) => x === category.id) > -1)
    .map((x) => x.description ? x.description.toLowerCase() : '');
}

// All sagas to be loaded
export default [
  watchSearch,
];
