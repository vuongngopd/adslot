/**
 *
 * Search Box
 *
 */

import React, { PropTypes } from 'react';
import styled from 'styled-components';

import Input from 'components/Input';
import SearchIcon from 'components/Icon/Search';

const Wrapper = styled.div`
  display: flex;
  flex-flow: row;
  justify-content: center;
  align-items: center;
  margin-top: 40px;
  padding-left: 20%;
  padding-right: 20%;
  @media (max-width: 640px) {
    padding: 10px 5% 10px 5%;
  }
`;

function SearchBox(props) {
  return (
    <Wrapper>
      <Input value={props.value} icon={<SearchIcon />} placeholder="Search Publishers" onChange={props.onChange} />
    </Wrapper>
  );
}

SearchBox.propTypes = {
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string,
};

export default SearchBox;
