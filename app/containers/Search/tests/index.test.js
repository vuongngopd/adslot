import React from 'react';
import { mount } from 'enzyme';

import TopBar from 'components/TopBar';
import Close from 'components/Icon/Close';
import { Search, mapDispatchToProps } from '../index';
import SearchBox from '../SearchBox';
import NoResult from '../NoResult';
import Result from '../Result';
import * as Actions from '../actions';

describe('<Search />', () => {
  it('should render TopBar with Close Icon and SearchBox', () => {
    const renderedComponent = mount(
      <Search Search={{ query: '' }} />
    );
    expect(renderedComponent.find(TopBar).length).toBe(1);
    expect(renderedComponent.find(Close).length).toBe(1);
    expect(renderedComponent.find(SearchBox).length).toBe(1);
  });

  it('should render NoResult', () => {
    const renderedComponent = mount(
      <Search Search={{ query: 'test', results: [] }} />
    );
    expect(renderedComponent.find(NoResult).length).toBe(1);
  });

  it('should render two result', () => {
    const mock = [
      { id: 1, siteUrl: 'www.test.com', description: 'Test' },
      { id: 2, siteUrl: 'www.test.com', description: 'Test' },
    ];
    const renderedComponent = mount(
      <Search Search={{ query: 'test', results: mock }} />
    );
    expect(renderedComponent.find(Result).length).toBe(2);
  });

  describe('mapDispatchToProps', () => {
    describe('query', () => {
      it('should be injected', () => {
        const dispatch = jest.fn();
        const result = mapDispatchToProps(dispatch);
        expect(result.query).toBeDefined();
      });

      it('should dispatch query when called', () => {
        const dispatch = jest.fn();
        const result = mapDispatchToProps(dispatch);
        const eMock = { preventDefault() {}, target: { value: 'test' } };
        result.query(eMock);
        expect(dispatch).toHaveBeenCalledWith(Actions.query('test'));
      });
    });
  });
});
