
import { fromJS } from 'immutable';
import searchReducer from '../reducer';
import {
  QUERY,
  SET_RESULTS,
} from '../constants';

describe('searchReducer', () => {
  it('returns the initial state', () => {
    expect(searchReducer(undefined, {}))
      .toEqual(fromJS({ query: '', results: [] }));
  });

  it('update state with query', () => {
    expect(searchReducer(undefined, { type: QUERY, str: 'test' }))
      .toEqual(fromJS({ query: 'test', results: [] }));
  });

  it('update state with results', () => {
    expect(searchReducer(undefined, { type: SET_RESULTS, results: [{ id: 1 }] })
           .get('results')).toEqual([{ id: 1 }]);
  });
});
