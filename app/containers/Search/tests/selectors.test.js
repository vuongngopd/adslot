import { fromJS } from 'immutable';
import makeSelectSearch, { selectSearchDomain, makeSelectQuery } from '../selectors';

describe('search selectors', () => {
  it('return from selectSearchDomain', () => {
    const queryState = fromJS({ query: '' });
    const mockedState = fromJS({ search: queryState });
    expect(selectSearchDomain()(mockedState)).toEqual(queryState);
  });

  it('return from makeSelectQuery', () => {
    const query = 'test';
    const mockedState = fromJS({
      search: { query },
    });
    expect(makeSelectQuery()(mockedState)).toEqual(query);
  });

  it('return from makeSelectSearch', () => {
    const query = 'test';
    const mockedState = fromJS({
      search: { query },
    });
    expect(makeSelectSearch()(mockedState)).toEqual({ query });
  });
});
