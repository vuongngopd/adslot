
import {
  query,
  setResults,
} from '../actions';
import {
  QUERY,
  SET_RESULTS,
} from '../constants';

describe('Search actions', () => {
  describe('query Action', () => {
    it('has a type of QUERY', () => {
      const expected = {
        type: QUERY,
        str: '',
      };
      expect(query()).toEqual(expected);
    });

    it('has string query in return ', () => {
      const expected = {
        type: QUERY,
        str: 'test',
      };
      expect(query('test')).toEqual(expected);
    });
  });

  describe('setResults Action', () => {
    it('has a type of SET_RESULTS', () => {
      const expected = {
        type: SET_RESULTS,
        results: [],
      };
      expect(setResults()).toEqual(expected);
    });

    it('has search results in return ', () => {
      const expected = {
        type: SET_RESULTS,
        results: [{ id: 1 }],
      };
      expect(setResults([{ id: 1 }])).toEqual(expected);
    });
  });
});
