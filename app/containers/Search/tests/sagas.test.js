/**
 * Test  sagas
 */

/* eslint-disable redux-saga/yield-effects */
import { put, takeEvery } from 'redux-saga/effects';
import { watchSearch, mockSearch, getSiteCategories, mergeSiteWithCategory, searchBySites } from '../sagas';
import { QUERY } from '../constants';
import { setResults } from '../actions';
import { sites, categories } from '../mock';
// const generator = defaultSaga();

describe('search Saga', () => {
  describe('Watch saga', () => {
    it('Should watch query change', () => {
      expect(watchSearch().next().value).toEqual(takeEvery(QUERY, mockSearch));
    });
  });

  describe('Mock generator', () => {
    it('call setResults with empty array', () => {
      const gen = mockSearch();
      gen.next();
      expect(gen.next('test').value).toEqual(put(setResults([])));
    });

    it('call setResults with one element in array', () => {
      const res = {
        id: 2,
        siteName: 'Ebay',
        siteUrl: 'www.ebay.com.au',
        description: 'This is the description for ebay',
        categoryIds: [
          1,
        ],
        categories: ['arts & entertainment'],
      };
      const gen = mockSearch();
      gen.next();
      expect(gen.next('ebay').value).toEqual(put(setResults([res])));
    });
  });

  describe('getSiteCategories', () => {
    it('return empty array', () => {
      expect(getSiteCategories({ categoryIds: [3] }, [{ id: 1 }, { id: 2 }]))
        .toEqual([]);
    });

    it('return empty array of 2', () => {
      expect(getSiteCategories({ categoryIds: [1, 3] }, [{ id: 1 }, { id: 2 }, { id: 3 }]).length)
        .toEqual(2);
    });
  });

  describe('mergeSiteWithCategory and lowercase category name', () => {
    it('merge', () => {
      const s = [{ categoryIds: [1, 3] }];
      const c = [{ id: 1, description: 'Test1' }, { id: 2, description: 'Test2' }, { id: 3 }];
      const res = [{ categoryIds: [1, 3], categories: ['test1', ''] }];
      expect(mergeSiteWithCategory(s, c)).toEqual(res);
    });
  });

  describe('searchBySites', () => {
    it('return site by match siteName', () => {
      const mock = mergeSiteWithCategory(sites, categories);
      const res = {
        categories: ['arts & entertainment'],
        categoryIds: [1],
        description: 'This is the description for ebay',
        id: 2,
        siteName: 'Ebay',
        siteUrl: 'www.ebay.com.au',
      };
      expect(searchBySites('ebay', mock)).toEqual([res]);
    });

    it('return site by match category', () => {
      const mock = mergeSiteWithCategory(sites, categories);
      const res = [{
        categories: ['arts & entertainment'],
        categoryIds: [1],
        description: 'This is the description for ebay',
        id: 2,
        siteName: 'Ebay',
        siteUrl: 'www.ebay.com.au',
      }, {
        categories: ['arts & entertainment', 'automotive', 'business', 'careers'],
        categoryIds: [1, 2, 3, 4],
        description: 'This is the description for Table Tennis Tips',
        id: 4,
        siteName: 'Table Tennis Tips - How to not come runners up',
        siteUrl: 'www.ttt.com',
      }];
      expect(searchBySites('arts', mock)).toEqual(res);
    });
  });
});
