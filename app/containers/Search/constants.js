/*
 *
 * Search constants
 *
 */
export const QUERY = 'app/Search/QUERY';
export const SET_RESULTS = 'app/Search/SET_RESULTS';
