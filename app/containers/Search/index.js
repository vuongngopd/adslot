/*
 *
 * Search
 *
 */

import React, { PropTypes } from 'react';
import { browserHistory } from 'react-router';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { createStructuredSelector } from 'reselect';

import TopBar from 'components/TopBar';
import IconButton from 'components/IconButton';
import CloseIcon from 'components/Icon/Close';

import makeSelectSearch from './selectors';
import * as Actions from './actions';

import SearchBox from './SearchBox';
import ResultList from './ResultList';

export class Search extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <div>
        <TopBar
          right={<div><IconButton icon={<CloseIcon />} onClick={() => browserHistory.push('/')} /></div>}
        />
        <Helmet
          title="Search"
          meta={[
            { name: 'description', content: 'Search by siteName or category' },
          ]}
        />
        <SearchBox value={this.props.Search.query} onChange={this.props.query} />
        {this.props.Search.query && <ResultList results={this.props.Search.results} />}
      </div>
    );
  }
}

Search.propTypes = {
  query: PropTypes.func.isRequired,
  Search: PropTypes.object.isRequired,
};

const mapStateToProps = createStructuredSelector({
  Search: makeSelectSearch(),
});

export function mapDispatchToProps(dispatch) {
  return {
    query: (e) => {
      e.preventDefault();
      dispatch(Actions.query(e.target.value));
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Search);
