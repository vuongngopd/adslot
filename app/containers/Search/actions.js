/*
 *
 * Search actions
 *
 */

import {
  QUERY,
  SET_RESULTS,
} from './constants';

/**
 * Dispatch query action when user type in search
 *
 * @param {str} string Query String
 *
 */
export function query(str = '') {
  return {
    type: QUERY,
    str,
  };
}

/**
 * Dispatch setResults action when saga get result either from API or mock
 *
 * @param {results} array Result objects
 *
 */
export function setResults(results = []) {
  return {
    type: SET_RESULTS,
    results,
  };
}
