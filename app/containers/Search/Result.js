/**
 *
 * Result
 *
 */

import React, { PropTypes } from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
  display: flex;
  flex-flow: column;
`;

const ALink = styled.a`
  font-size: 20px;
  font-weight: 300;
  text-decoration: none;
  color: #006dcc;
  border-bottom: 1px solid #006dcc;
`;

const TextLink = styled.span`
  position: relative;
  bottom: -4px;
`;

const P = styled.p`
  margin-top: 4px;
  color: #6c6e6f;
  font-weight: 300;
  margin-top: 0px;
  font-size: 14px;
  word-wrap: break-word;
  white-space: pre-line;
`;


function Result(props) {
  return (
    <Wrapper>
      <div>
        <ALink href={props.siteUrl}><TextLink>{props.siteUrl}</TextLink></ALink>
        <P>{props.description}</P>
      </div>
    </Wrapper>
  );
}

Result.propTypes = {
  siteUrl: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
};

export default Result;
