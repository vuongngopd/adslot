/**
 *
 * No Result
 *
 */

import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
  display: flex;
  flex-flow: column;
  margin-top: 20px;
  justify-content: center;
  align-items: center;
`;

const P = styled.p`
  color: #919294;
  font-weight: 300;
  font-size: 20px;
  border: 1px solid #d0d1d2;
  padding: 10px 25px 10px 25px;
`;


function NoResult() {
  return (
    <Wrapper>
      <P>We currently don't have any results for your search, try another.</P>
    </Wrapper>
  );
}

NoResult.propTypes = {
};

export default NoResult;
