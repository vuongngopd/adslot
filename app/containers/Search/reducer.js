/*
 *
 * Search reducer
 *
 */

import { fromJS } from 'immutable';
import {
  QUERY,
  SET_RESULTS,
} from './constants';

const initialState = fromJS({
  query: '',
  results: [],
});

function searchReducer(state = initialState, action) {
  switch (action.type) {
    case QUERY:
      return state
        .set('query', action.str);
    case SET_RESULTS:
      return state
        .set('results', action.results);
    default:
      return state;
  }
}

export default searchReducer;
