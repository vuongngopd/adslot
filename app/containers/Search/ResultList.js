/**
 *
 * Results List
 *
 */

import React, { PropTypes } from 'react';
import styled from 'styled-components';

import Result from './Result';
import NoResult from './NoResult';

const Wrapper = styled.div`
  display: flex;
  flex-flow: column;
  margin-top: 20px;
  padding-left: 20%;
  padding-right: 20%;
  @media (max-width: 640px) {
    padding-left: 5%;
    padding-right: 5%;
  }
`;

function ResultList(props) {
  return (
    <Wrapper>
      { props.results.length > 0 ?
        props.results.map((result) => <Result key={result.id} {...result} />)
        :
        <NoResult />
      }
    </Wrapper>
  );
}

ResultList.propTypes = {
  results: PropTypes.array.isRequired,
};

export default ResultList;
