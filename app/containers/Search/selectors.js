import { createSelector } from 'reselect';

/**
 * Direct selector to the search state domain
 */
const selectSearchDomain = () => (state) => state.get('search');


/**
 * Query selector used by Search
 */

const makeSelectQuery = () => createSelector(
  selectSearchDomain(),
  (searchState) => searchState.get('query')
);

/**
 * Default selection
 */
const makeSelectSearch = () => createSelector(
  selectSearchDomain(),
  (searchState) => searchState.toJS()
);

export default makeSelectSearch;
export {
  selectSearchDomain,
  makeSelectQuery,
};
