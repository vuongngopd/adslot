import styled from 'styled-components';

const SVGWrapper = styled.svg`
  display: inline-block;
  width: 1em;
  height: 1em;
  stroke-width: 0;
  stroke: grey;
  fill: grey;
  width: 0.7861328125em;
`;

export default SVGWrapper;

