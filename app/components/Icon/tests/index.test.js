import React from 'react';
import { shallow } from 'enzyme';

import { Close, Search } from '../index';
import Wrapper from '../Wrapper';

describe('<Icon />', () => {
  it('should render Btn with icon', () => {
    const renderedComponent = shallow(
      <Close />
    );
    expect(renderedComponent.find(Wrapper).length).toBe(1);
  });

  it('should render Btn with label', () => {
    const renderedComponent = shallow(
      <Search />
    );
    expect(renderedComponent.find(Wrapper).length).toBe(1);
  });
});
