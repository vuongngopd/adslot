/**
*
* TopBar
*
*/

import React from 'react';
import { browserHistory } from 'react-router';
import styled from 'styled-components';

import MainLogo from 'components/MainLogo';


const Bar = styled.div`
  max-width: 100%;
  padding: 10px 20% 10px 20%;
  border: 1px grey solid;
  min-height: 64px;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  background-color: white;
  @media (max-width: 640px) {
    padding: 10px 5% 10px 5%;
  }
`;

const Logo = styled.div`
  width: 125px;
  cursor: pointer;
`;

const RightBtns = styled.div`
  display: flex;
  flex-direction: row;
`;

class TopBar extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <Bar>
        <Logo onClick={() => browserHistory.push('/')}>
          <MainLogo />
        </Logo>
        <RightBtns>
          {this.props.right}
        </RightBtns>
      </Bar>
    );
  }
}

TopBar.propTypes = {
  right: React.PropTypes.node,
};

export default TopBar;
