import React from 'react';
import { shallow } from 'enzyme';

import MainLogo from 'components/MainLogo';
import TopBar from '../index';

describe('<TopBar />', () => {
  it('should render TopBar with Logo', () => {
    const renderedComponent = shallow(
      <TopBar />
    );
    expect(renderedComponent.find(MainLogo).length).toBe(1);
  });

  it('should render TopBar with right element', () => {
    const renderedComponent = shallow(
      <TopBar right={<h1>Hi</h1>} />
    );
    expect(renderedComponent.contains(<h1>Hi</h1>)).toBe(true);
  });
});
