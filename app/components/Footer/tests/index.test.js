import React from 'react';
import { shallow } from 'enzyme';

import MainLogo from 'components/MainLogo';
import Footer from '../index';
import Links from '../Links';

describe('<Footer />', () => {
  it('should render the logo', () => {
    const renderedComponent = shallow(
      <Footer />
    );
    expect(renderedComponent.find(MainLogo).length).toBe(1);
  });

  it('should render links', () => {
    const renderedComponent = shallow(<Footer />);
    expect(renderedComponent.find(Links).length).toBe(1);
  });
});
