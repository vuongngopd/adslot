import styled from 'styled-components';

export const LogoWrapper = styled.div`
  width: 150px;
`;

const Wrapper = styled.footer`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  padding: 3em 0;
  border-top: 1px solid #666;
  background-color: #3a383a;
`;

export default Wrapper;
