import React from 'react';

import MainLogo from 'components/MainLogo';
import Divider from './Divider';
import Wrapper, { LogoWrapper } from './Wrapper';
import Links from './Links';

function Footer() {
  return (
    <Wrapper>
      <LogoWrapper>
        <MainLogo inverse />
      </LogoWrapper>
      <Divider />
      <section>
        <Links />
      </section>
    </Wrapper>
  );
}

export default Footer;
