import styled from 'styled-components';

const Divider = styled.footer`
  width: 80%;
  border-bottom: 1px solid #666;
  margin: 20px;
`;

export default Divider;
