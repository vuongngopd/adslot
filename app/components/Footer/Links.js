import React from 'react';
import { Link } from 'react-router';
import styled from 'styled-components';

import { links } from './constants';

const Wrapper = styled.div`
  display: inline-flex ;
  flex-wrap: wrap;
  justify-content: center;
`;

const LinkWrapper = styled.div`
  color: #919294;
  display: inherit;
  margin-top: 10px;
`;

const FLink = styled(Link)`
  color: #919294;
  text-decoration: none;
  font-size: 16px;
  font-weight: 300;
  border-left: 1px solid #919294;
  padding-left: 6px;
  margin-right: 6px;
  &:hover{
    color: #006dcc;
  }
`;

function Links() {
  return (
    <Wrapper>
      {links.map((link, i) =>
        <LinkWrapper key={link.name}>
          <FLink style={i === 0 ? { borderLeft: 'none' } : null} to={link.to}>
            {link.name}
          </FLink>
        </LinkWrapper>)}
    </Wrapper>
  );
}

export default Links;
