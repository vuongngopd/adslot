/**
*
* Input
*
*/

import React, { PropTypes } from 'react';
import styled from 'styled-components';

export const InputWrapper = styled.div`
  border: 1px solid grey;
  display: flex;
  flex-flow: row;
  justify-content: space-between;
  align-items: center;
  min-height: 50px;
  width: 100%;
  padding: 18px;
  background-color: white;
`;

export const FieldWrapper = styled.input`
  font-family: 'Roboto', sans-serif;
  width: 95%;
  font-size: 24px;
  font-weight: 300;
  &:focus {
    font-weight: 300;
    border: none;
  }
`;

export const IconWrapper = styled.div`
  width: 20px;
  font-size: 24px;
  display: flex;
  align-items: center;
`;

function Input(props) {
  return (
    <InputWrapper>
      <FieldWrapper value={props.value} placeholder={props.placeholder} onChange={props.onChange} />
      <IconWrapper>{props.icon}</IconWrapper>
    </InputWrapper>
  );
}

Input.propTypes = {
  placeholder: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  icon: PropTypes.object,
  value: PropTypes.string,
};

export default Input;
