import React from 'react';
import { mount } from 'enzyme';

import Input, { FieldWrapper, IconWrapper } from '../index';

describe('<Input />', () => {
  it('Expect to render logo image', () => {
    const spy = jest.fn();
    const renderedComponent = mount(
      <Input placeholder="test" onChange={spy} icon={<h1>Test</h1>} />
    );
    expect(renderedComponent.find(FieldWrapper).length).toBe(1);
    expect(renderedComponent.contains(<IconWrapper><h1>Test</h1></IconWrapper>)).toBe(true);

    renderedComponent.find('input').simulate('change', { target: { value: 'test' } });
    expect(spy).toHaveBeenCalled();
  });
});
