import React from 'react';
import { shallow } from 'enzyme';

import Img from 'components/Img';
import MainLogo from '../index';

describe('<MainLogo />', () => {
  it('Expect to render logo image', () => {
    const renderedComponent = shallow(
      <MainLogo />
    );
    expect(renderedComponent.find(Img).length).toBe(1);
  });
});
