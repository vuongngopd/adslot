/**
*
* MainLogo
*
*/

import React, { PropTypes } from 'react';
// import styled from 'styled-components';

import Img from 'components/Img';
import Logo from './logo.png';
import Inverse from './inverse.png';

function MainLogo(props) {
  return (
    <Img src={props.inverse ? Inverse : Logo} alt="Adslot" className="logo" onClick={props.onClick} />
  );
}

MainLogo.propTypes = {
  inverse: PropTypes.bool,
  onClick: PropTypes.func,
};

export default MainLogo;
