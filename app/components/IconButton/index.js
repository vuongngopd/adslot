/**
*
* IconButton
*
*/

import React, { PropTypes } from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
  border: 1px solid grey;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  padding: 3px;
  cursor: pointer;
  transition: all 1s ease;
  &:hover{
    background-color: #f5f5f5;
    border: 1px solid #C0C0C0;
  }
`;

const IconWrapper = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  margin-left: 6px;
  margin-right: -2px;
`;

const Label = styled.div`
  color: grey;
  font-size: 16px;
  margin: 0px 4px 0px 6px;
`;

function IconButton(props) {
  return (
    <Wrapper onClick={props.onClick}>
      <IconWrapper>
        {props.icon}
      </IconWrapper>
      <Label>
        {props.label}
      </Label>
    </Wrapper>
  );
}

IconButton.propTypes = {
  icon: PropTypes.object.isRequired,
  label: PropTypes.string,
  onClick: PropTypes.func.isRequired,
};

export default IconButton;
