import React from 'react';
import { shallow } from 'enzyme';

import IconButton from '../index';

describe('<IconButton />', () => {
  it('should render Btn with icon', () => {
    const renderedComponent = shallow(
      <IconButton icon={<img src="www.test.com" alt="test" />} />
    );
    expect(renderedComponent.contains(<img src="www.test.com" alt="test" />)).toBe(true);
  });

  it('should render Btn with label', () => {
    const renderedComponent = shallow(
      <IconButton label={<h1>Click</h1>} />
    );
    expect(renderedComponent.contains(<h1>Click</h1>)).toBe(true);
  });
});
